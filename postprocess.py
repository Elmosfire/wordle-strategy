import json
import requests

tree = {}

with open("output.txt") as file:
    for x in file:
        x = x.strip()
        if not x.endswith("+++"):
            continue
        
        x = x.replace("X","⬜").replace("O","🟨").replace("V","🟩")
        
        
        a,b = x.split(' -> ')
        a = a.split(":")
        c = a[-1]
        a = a[:-1]
        if c == "easy":
            mode = "EASYMODE"
        elif c == "hard":
            mode = "HARDMODE"
        else:
            assert False, a
        
        key = ":".join([mode] + a + [b])
        s = tree
        for y in key.split("-"):
            ry = y.replace(" +++", "")
            if ry not in s:
                if not y.endswith("+++"):
                    s[ry] = {}
                else:
                    s[ry] = '+++'
            s = s[ry]

        
tree_text = json.dumps(tree)

with open("output.json", 'w') as file:
    file.write(tree_text)
    

resp = requests.get('http://www.bodurov.com/JsonFormatter/')
if resp.ok:
    html_page = resp.text
else:
    assert False, "invalid"

html_page = html_page.replace("""  <textarea id="RawJson">\r\n  </textarea>""", f"""<div id="RawJson" hidden>{tree_text}</div>""")
html_page = html_page.replace("""var json = $id("RawJson").value;""", f"""var json = $id("RawJson").innerHTML;""")
html_page = html_page.replace("""<h3 id="HeaderSubTitle">""","""<h3 id="HeaderSubTitle" hidden>""")
html_page = html_page.replace("""<div>Enter your JSON here:""","""<div hidden>Enter your JSON here:""")
html_page = html_page.replace("""http://bodurov.com/jsonformatter/images""","""https://elmosfire.gitlab.io/wordle-strategy""")
html_page = html_page.replace(""" value="Format" """,""" value="Refresh" """)
html_page = html_page.replace("""<body>""", """<body onload="Process()">""")


with open("public/output.html", 'w') as file:
    file.write(html_page)