use load_file::load_str;
use cached::proc_macro::cached;
use indicatif::ParallelProgressIterator;
use rayon::prelude::*;
use measure_time::{info_time, debug_time, trace_time, error_time, print_time};
use std::fs::File;
use std::io::{Error, Write};

enum FeedbackBit {
    CORRECT,
    PRESENT,
    ABSENT
}

#[derive(PartialEq)]
struct Feedback {
    id_:u8
}

fn compress(data: Vec<FeedbackBit>) -> Feedback {
    let mut i = 0;
    for x in data {
        i *= 3;
        i += match x {
            FeedbackBit::CORRECT => 0,
            FeedbackBit::PRESENT => 1,
            FeedbackBit::ABSENT => 2
        };
    };
    Feedback{id_:i}
}

fn compare( word1: &String, word2: &String) -> Feedback {
    let vec1: Vec<char> = word1.chars().collect();
    let vec2: Vec<char> = word2.chars().collect();
    let indx1: Vec<usize> = (0..5).map(|x| vec1.clone().into_iter().take(x).filter(|&y| y == vec1[x]).count()).collect();
    let indx2: Vec<usize> = (0..5).map(|x| vec1.clone().into_iter().filter(|&y| y == vec2[x]).count()).collect();
    compress((0..5).map(|x| {
        if vec1[x] == vec2[x] 
        {
            FeedbackBit::CORRECT
        }
        else if word2.contains(vec1[x]) && indx2[x] >= indx1[x]
        {
            FeedbackBit::PRESENT
        }
        else
        {
            FeedbackBit::ABSENT
        }
    }).collect())
}

#[cached]
fn words() -> Vec<String> {
    load_str!("../wordlist.txt").split("\n").map(|x| x.to_string()).collect()
}

#[cached]
fn words_sol() -> Vec<String> {
    load_str!("../sollist3.txt").split("\n").map(|x| x.to_string()).collect()
}

fn words_cmp(word: String) -> Vec<Feedback> {
    words_sol().iter().map(|x| compare(&word,x)).collect()
}



struct SolutionSet {
    mask : Vec<bool>
}

impl SolutionSet {

    fn default() -> SolutionSet {
        SolutionSet{mask:words_sol().iter().map(|_x| true).collect()}
    }
    fn combine(&self, other: &SolutionSet) -> SolutionSet {
        SolutionSet{mask:self.mask.iter().zip(other.mask.iter()).map(|(x,y)| x & y).collect()}
    }
    fn count(&self) -> usize {
        self.mask.iter().filter(|&&x| x).count()
    }
    fn words(&self) -> Vec<String> {
        words_sol().iter().zip(self.mask.iter()).filter(|(_x,&y)| y).map(|(x,_y)| x.to_string()).collect()
    }

    fn only(&self) -> Option<String> {
        if self.count() == 1 {
            let index = self.mask.iter().take_while(|&&x| !x).count();
            Some(words_sol()[index].to_string())
        }
        else
        {
            None
        }
    }
}


struct PartitionBase {
    opts : Vec<SolutionSet>,
    name : String
}

impl PartitionBase {
    fn new(word1: String) -> PartitionBase {
        let cmp = words_cmp(word1.to_string());
        
        let result = PartitionBase{
                opts: (0..243).map(|x| {
                SolutionSet{mask:cmp.iter().map(|y| y.id_ == x).collect()}
            }).collect(),
            name: word1
        };
        result.verify_outer();
        result
    }

    fn verify_outer(&self) {
        assert_eq!(self.values().sum::<usize>(), SolutionSet::default().count(), "{}", self.name);
    }

    fn sieve(&self, sieve: &SolutionSet) -> PartitionBase {
        PartitionBase{
            opts: self.opts.iter().map(|x| x.combine(sieve)).collect(),
            name: self.name.to_string()
        }
    }

    fn values(&self) -> impl Iterator<Item=usize> + '_ {
        self.opts.iter().map(|x| x.count())
    }

    fn worst_case(&self) -> Option<usize> {
        let mx = self.values().max()?;
        self.opts.iter().position(|x| x.count() == mx)
    }

    fn valid(&self) -> bool {
        self.opts[0].count() > 0
    }

    fn heuristic(&self) -> (usize, i128) {
        (self.values().max().unwrap_or(0), -(self.values().filter(|&x| x > 0).count() as i128))
    }

    fn heuristic_euler(&self) -> u128 {
        self.values().map(|x| x*x).sum::<usize>() as u128
    }
}


struct PartitionAtlas {
    partitions: Vec<PartitionBase>
}

impl PartitionAtlas {
    fn build() -> PartitionAtlas {
        print_time!("loading atlas");
        
        PartitionAtlas{partitions:words().par_iter().progress().map(|x| PartitionBase::new(x.to_string())).collect()}

        
    }

    fn verify(&self) {
        for x in &self.partitions {
            x.verify_outer();
        }
    }

    fn find(&self, name: String) -> Option<&PartitionBase> {
        self.partitions.iter().filter(|x| x.name == name).next()
    }

    fn perfect(&self, sieve: &SolutionSet) -> Option<PartitionBase> {
        print_time!("scan ");
        self.partitions.par_iter().progress().map(|x| x.sieve(sieve)).filter(|x| x.valid()).find_any(|x| x.values().max() == Some(1))
    }

    fn optimise(&self, sieve: &SolutionSet) -> Option<PartitionBase> {
        print_time!("optimise ");
        if sieve.count() < 50 {
            for x in self.perfect(sieve) {
                return Some(x);
            }
        }
        self.partitions.par_iter().progress().map(|x| x.sieve(sieve)).min_by_key(|x| x.heuristic())
    }

    fn optimise_hardmode(&self, sieve: &SolutionSet) -> Option<PartitionBase> {
        print_time!("optimise hardmode");
        if sieve.count() < 50 {
            for x in self.perfect(sieve) {
                return Some(x);
            }
        }
        self.partitions.par_iter().progress().map(|x| x.sieve(sieve)).filter(|x| x.valid()).min_by_key(|x| x.heuristic())
    }

    fn print_guess(&self, sieve: &SolutionSet ) -> () {
        for x in self.partitions.iter().map(|x| x.sieve(sieve)) {
            println!("{:<16} {:03} {:03}", x.name, x.heuristic().0, x.heuristic().1)
        }
        
    }

    fn print_guess_nosieve(&self, name: String) -> () {
        for x in self.partitions.iter() {
            if x.name == name {
                println!("{:},{:04},{:04},{}", x.name, x.heuristic().0, x.heuristic().1, format_guess(&x.worst_case().unwrap()))
            }
        }
        
    }
}


fn format_guess(i: &usize) -> String {
    let mut result = vec![];
    let mut x = *i;
    let res: Vec<char> = "VOX".chars().collect();

    for _ in 0..5 {
        let m = x % 3;
        x = x / 3;
        result.push(res[m]);
    }
    result.into_iter().rev().collect()
}



fn log_nodes(con: &mut redis::Connection, output: &mut File, atlas: &PartitionAtlas, sieve: &SolutionSet , past: Vec<(String, usize)>, hardmode: bool, counter: &mut usize) -> Result<(), Error> {

    let formatted_past = {
        past.iter().map(|(x,y)| format!("{}-{}", x, format_guess(y))).fold(String::new(), |a, b| a + &b + ":") + if hardmode {"hard"} else {"easy"}
    };
    let formatted_past_old = {
        formatted_past.to_string() //if changing the formatted past, put the old version here and runs once to update the cache
    };
    if sieve.count() > 1 {
        let opt = match redis::cmd("GET").arg(formatted_past.to_string()).query::<String>(con).ok() {
            Some(x) => atlas.find(x).unwrap().sieve(sieve), 
            None => {
                let res = match redis::cmd("GET").arg(formatted_past_old.to_string()).query::<String>(con).ok() 
                    {
                        Some(x) => atlas.find(x).unwrap().sieve(sieve),
                        None => {
                        if hardmode {
                            atlas.optimise_hardmode(sieve).unwrap().sieve(sieve)
                        } else {
                            atlas.optimise(sieve).unwrap().sieve(sieve)
                        }
                    }
                };
                redis::cmd("SET").arg(formatted_past.to_string()).arg(res.name.to_string()).query::<String>(con).ok().unwrap();
                res
            }
        };
        assert_eq!(opt.values().sum::<usize>(), sieve.count());
        let n = past.iter().map(|x| x.1).fold(String::new(), |a, b| a + &format!("{}",b) + ".");

        println!("open_: {:03} {} {}/{}", n, opt.name, counter, sieve.count());
        writeln!(output, "{} -> {} ({}|{})", formatted_past, opt.name, sieve.count(), opt.values().max().unwrap());
        
        for (i,x) in opt.opts.iter().enumerate() {
            let mut pastcopy = past.clone();
            pastcopy.push((opt.name.to_string(), i));
            log_nodes(con, output, atlas, &x, pastcopy , hardmode, counter)?;
        };
        println!("close: {:03} {} {}/{}", n, opt.name, counter, sieve.count());

        return Ok(())

    }
    else
    {
        match sieve.only() {
            Some(best) => {writeln!(output, "{} -> {} +++", formatted_past, best); *counter += 1;},
            None => (),
        };
        return Ok(())
    }

}

fn log_nodes_wrapper() -> Result<(), Error> {
    let client = redis::Client::open("redis://127.0.0.1/").ok().unwrap();
    let mut con = client.get_connection().ok().unwrap();
    let p = PartitionAtlas::build();
    let default = SolutionSet::default();
    let mut output = File::create("output.txt")?;
    log_nodes(&mut con, &mut output, &p, &default, Vec::new(), false, &mut 0)?;
    log_nodes(&mut con, &mut output, &p, &default, Vec::new(), true, &mut 0)
}

    

pub fn process() {
    print_time!("full process");
    log_nodes_wrapper().ok();
    
}




    
